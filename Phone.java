package ua.ost.chayka.tetyana.tommy.mobilephone;

public abstract class Phone {
    private String nazva, model;
    private int objem, oper;

    public Phone(String nazva, String model, int objem, int oper) {
        this.nazva = nazva;
        this.model = model;
        this.objem = objem;
        this.oper = oper;
    }

    private String getNazva() {
        return nazva;
    }

    private void setNazva(String nazva) {
        this.nazva = nazva;
    }

    private String getModel() {
        return model;
    }


    private void setModel(String model) {
        this.model = model;
    }

    private int getObjem() {
        return objem;
    }

    private void setObjem(int objem) {
        this.objem = objem;
    }

    private int getOper() {
        return oper;
    }

    private void setOper(int oper) {
        this.oper = oper;
    }


    public String toString() {
        return "Телефон" + nazva + "; Модель:" + model + "; " +
                "Об'єм пам'яті: " + objem + "Гб; Оперативна пам'ять:" +
                oper + "Гб.";
    }

}
