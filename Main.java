package ua.ost.chayka.tetyana.tommy.mobilephone;

public class Main {
    public static void main(String[] args) {
        ShowTelephon(new XiaomiRedmiPhone("XiaomiRedmi",
                "Note11", 128, 8, 50));
        PhoneConnection con1;
        con1 = new XiaomiRedmiPhone("XiaomiRedmi",
                "Note11", 128, 8, 50);
        con1.connection();
        PhoneMedia med1;
        med1 = new XiaomiRedmiPhone("XiaomiRedmi",
                "Note11", 128, 8, 50);
        med1.media();
        ShowTelephon(new SamsungPhone("Samsung", "Galaxy Flip 4",
                256, 8, 24));
        PhoneConnection con2;
        con2 = new SamsungPhone("Samsung", "Galaxy Flip 4",
                256, 8, 24);
        con2.connection();
        PhoneMedia med2;
        med2 = new SamsungPhone("Samsung", "Galaxy Flip 4",
                256, 8, 24);
        med2.media();
        ShowTelephon(new NokiaPhone("Nokia", "G11 Plus",
                64, 4));
        PhoneConnection con3;
        con3 = new NokiaPhone("Nokia", "G11 Plus", 64, 4);
        con3.connection();
    }

    public static void ShowTelephon(Phone phone) {

        System.out.println(phone.toString());
    }

}

