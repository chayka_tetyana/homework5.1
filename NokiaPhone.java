package ua.ost.chayka.tetyana.tommy.mobilephone;

public class NokiaPhone extends Phone implements PhoneConnection {
    public NokiaPhone(String nazva, String model, int objem, int oper) {
        super(nazva, model, objem, oper);
    }

    @Override
    public void connection() {
        System.out.println("Я можу телефонувати і надсилати повідомлення, " +
                "але не можу фотографувати і знімати відео");
    }
}
