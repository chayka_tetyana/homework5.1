package ua.ost.chayka.tetyana.tommy.mobilephone;

public class XiaomiRedmiPhone extends Phone implements PhoneConnection, PhoneMedia {
    private int rozhyrennya;

    public XiaomiRedmiPhone(String nazva, String model, int objem, int oper, int rozhyrennya) {

        super(nazva, model, objem, oper);
        this.rozhyrennya = rozhyrennya;
    }

    public int getRozhyrennya() {
        return rozhyrennya;
    }

    public void setRozhyrennya(int rozhyrennya) {
        this.rozhyrennya = rozhyrennya;
    }

    @Override
    public void connection() {
        System.out.println("Я можу телефонувати і надсилати повідомлення, і мені подобається це робити");
    }

    @Override
    public void media() {
        System.out.println("Я гарно вмію робити фото і знімати відео");
        System.out.println("Розширення камери:" + rozhyrennya + "Мп.");
    }

}
